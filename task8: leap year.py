
def isleapyear(year):
    if year % 400 == 0:
        return True
    elif year % 100 == 0:
        return False
    elif year % 4 == 0:
        return True
    return False

print(isleapyear(2400))
print(isleapyear(2200))
print(isleapyear(2003))
print(isleapyear(1990))