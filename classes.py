from classeslib.animal import animal, cat, dog


bill = animal('bill', 28)
bill.meet()
jim = dog('jim', 2.5)
mil = cat('mil', 1.2)


jim.meet()
mil.meet()