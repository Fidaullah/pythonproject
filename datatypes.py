import sys
A = 1
B = True
C = "this is string"
D = {"fida": "noonari"}
E = [A, B, C]
F = (1, 2, 3)
G = None
H = set(F)
print("A ", type(A))
print("B ", type(B))
print("C ", type(C))
print("D ", type(D))
print("E ", type(E))
print("F ", type(F))
print("G ", type(G))
print("H ", type(H))
sys.maxsize()