import random

def question():
    a, b = random.randint(0, 9), random.randint(0, 9)
    ans = a * b
    return a, b, ans

def main():
    while True:
        a, b, ans = question()
        while ans:
            value = input(f"{a} times {b} is equal to : ")
            if value == ans: 
                print ("very good")
