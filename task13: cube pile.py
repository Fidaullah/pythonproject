

def cube_pile(blocks):
    last = float('inf')
    while len(blocks) > 0:
        if blocks[0] > blocks[-1]:
            temp = blocks.pop(0)
        else:
            temp = blocks.pop(-1)
        if temp > last:
            return "No"
        last = temp
    return "yes"

blocks = [4, 3, 2, 1, 3, 4]
print(cube_pile(blocks))
blocks = [1, 3, 2]
print(cube_pile(blocks))
    
