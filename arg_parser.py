import argparse
# https://docs.python.org/3/library/argparse.html

parser = argparse.ArgumentParser(prog='my program', description='this is just a basic arg parser program to understand its functionality'
                                )
# this is sample optional argument
# '-' before the key letter '--' before the keyword signifyies the optional arguments
verbose_group = parser.add_mutually_exclusive_group()
verbose_group.add_argument('-v','--verbose', action='store_true', help='use this to print detailed output')
verbose_group.add_argument('-q','--quiet', action='store_true', help='use this to remove any output')

# positional argument
parser.add_argument('number', nargs=2, help='input to numbers ', type=int)
arg = parser.parse_args()
ans = sum(arg.number)
if arg.verbose:
    print("this is longer version of the output")
    print("sum of {} and {} is equal to {}".format(arg.number[0], arg.number[1], ans))
elif arg.quiet:
    print(ans)
else:
    print("{}+{}={}".format(arg.number[0], arg.number[1], ans))