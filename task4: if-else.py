
a = 4

def weird(value):
    if value % 2 == 1:
        print("Weird")
    elif value > 1 and value < 6 :
        print("Not Weird")
    elif value < 21:
        print("Weird")
    elif value > 20:
        print("Not Weird")
    
weird(5)
weird(13)
weird(24)