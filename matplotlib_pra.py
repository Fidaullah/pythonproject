import matplotlib.pyplot as plt
# for help go to this link: https://towardsdatascience.com/introduction-to-matplotlib-in-python-5f5a9919991f

#create data for plotting
x_values = [0,1,2,3,4,5]
squares = [0,1,4,9,16,25]
#the default graph style for plot is a line
plt.plot(x_values, squares)
#display the graph
plt.show()

x_values = [5,6,3,7,2]
y_values = ["A", "B", "C", "D", "E"]
plt.bar(y_values, x_values, color = "red")
plt.show()

# plt.scatter plt.barh plt.hist
