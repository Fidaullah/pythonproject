from collections import namedtuple

total = 0
N = int(input())
columns = input()
student = namedtuple('student', columns)
for i in range(0,N):
    values = input().split()
    values = student(values[0], values[1], values[2], values[3])
    total += int(values.MARKS)
average = float(total / N)
print(f"average marks are: {average:.2f}")
