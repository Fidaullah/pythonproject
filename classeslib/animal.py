class animal:
    'the is parent class of all animals'

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def whatnoise(self):
        print("makes weird unrecoginized noise")

    def meet(self):
        print("meet my new pet {}, he is {} years old".format(self.name, self.age))

class cat(animal):

    def __init__(self, name, age) -> None:
        super().__init__(name, age)
        self.species = 'cat'

    def whatnoise(self):
       print("meoow")
    
    def meet(self):
        super().meet()
        print("this is lovely cat and makes noise like")
        self.whatnoise()

class dog(animal):
    'this is child class of animal  for cats'

    def __init__(self, name, age) -> None:
        super().__init__(name, age)
        self.species = 'dog'

    def whatnoise(self):
       print("whaooo")

    def meet(self):
        super().meet()
        print("this is lovely dog and makes noise like")
        self.whatnoise()