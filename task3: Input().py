# this function verifies polynomial 
# task is define at following link
# https://www.hackerrank.com/challenges/input/problem

# input_A = input("please enter x and k").split(" ")
# input_B = input("please enter the polynomial").split(" ")
input_A = "1 4".split(" ")
input_B = "x**3 + x**2 + x + 1"

x = int(input_A[0])
k = int(input_A[1])
# answer = eval(input("enter the expressions  "))
answer = eval(input_B)
print(answer == k)
exit()

# convert polynomial to standard form x**2 + 2*x +3 to [1, 2, 3]
for item in input_B:
    print(item)
    if item == '+':
        continue
    item = item.replace(" ", "").replace("*", "").split("x")
   
    print(item)
print(input_B)
